package com.benenesyildirim.flicktagram;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.benenesyildirim.flicktagram.Adapter.EndlessRecyclerViewScrollListener;
import com.benenesyildirim.flicktagram.Adapter.PhotoListAdapter;
import com.benenesyildirim.flicktagram.Api.BaseUrl;
import com.benenesyildirim.flicktagram.Api.RequestInterface;
import com.benenesyildirim.flicktagram.Model.MainData;
import com.benenesyildirim.flicktagram.Model.PhotoMainData;
import com.benenesyildirim.flicktagram.Model.PhotoModel;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ListFragment extends Fragment implements PhotoListAdapter.ItemClickListener {

    List<PhotoModel> photosResult = new ArrayList<>();
    RecyclerView photoListRV;
    PhotoListAdapter photoListAdapter;
    View view;
    EndlessRecyclerViewScrollListener scrollListener;
    int page = 1;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_list, container, false);

        photoListRV = view.findViewById(R.id.latest_post_rv_listfragment);

        getData(page, true);

        return view;
    }

    private void fillRecyclerView() {
        photoListAdapter = new PhotoListAdapter(photosResult, ListFragment.this);

        LinearLayoutManager llm = new LinearLayoutManager(getContext());
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        photoListRV.setLayoutManager(llm);
        photoListRV.setAdapter(photoListAdapter);

        scrollListener = new EndlessRecyclerViewScrollListener(llm) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                Toast.makeText(getContext(), "Hele Hele", Toast.LENGTH_SHORT).show();

                getData(page++, false);
            }
        };

        photoListRV.addOnScrollListener(scrollListener);
        photoListAdapter.notifyDataSetChanged();
        scrollListener.resetState();
    }

    private void getData(int pageNumber, boolean isFirstLoad) {
        RequestInterface requestInterface = BaseUrl.getClient().create(RequestInterface.class);

        Call<MainData> call = requestInterface.getRecentPhotos(pageNumber);
        call.enqueue(new Callback<MainData>() {
            @Override
            public void onResponse(Call<MainData> call, Response<MainData> response) {
                photosResult.addAll(response.body().getPhotos().getPhoto());
                Toast.makeText(getContext(), "Hele Hele", Toast.LENGTH_SHORT).show();

                if (!isFirstLoad)
                    photoListAdapter.notifyDataSetChanged();
                else
                    fillRecyclerView();


            }

            @Override
            public void onFailure(Call<MainData> call, Throwable t) {
                Toast.makeText(getContext(), "Nope!", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onItemClick(PhotoModel clickedPhoto) {
        Bundle bundle = new Bundle();
        bundle.putString("id", clickedPhoto.getId());
        bundle.putString("secret", clickedPhoto.getSecret());
        bundle.putString("title", clickedPhoto.getTitle());
        Navigation.findNavController(view).navigate(R.id.action_listFragment_to_detailFragment, bundle);
        Toast.makeText(getContext(), "dddd", Toast.LENGTH_SHORT).show();
    }
}