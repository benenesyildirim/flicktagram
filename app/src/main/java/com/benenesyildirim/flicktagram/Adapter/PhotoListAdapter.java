package com.benenesyildirim.flicktagram.Adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.benenesyildirim.flicktagram.Model.PhotoMainData;
import com.benenesyildirim.flicktagram.Model.PhotoModel;
import com.benenesyildirim.flicktagram.R;
import com.squareup.picasso.Picasso;

import java.util.List;

public class PhotoListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<PhotoModel> photosList;
    private ItemClickListener itemClickListener;

    public PhotoListAdapter(List<PhotoModel> photosList, ItemClickListener itemClickListener) {
        this.photosList = photosList;
        this.itemClickListener = itemClickListener;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int pos) {
        LayoutInflater inflater = (LayoutInflater) viewGroup.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        @SuppressLint("InflateParams") View view = inflater.inflate(R.layout.list_item_design, null);
        return new ItemHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, @SuppressLint("RecyclerView") final int i) {
        ItemHolder itemHolder = (ItemHolder) viewHolder;
        String uri = "https://farm5.staticflickr.com/4218/" + photosList.get(i).getId() + "_" + photosList.get(i).getSecret() + ".jpg";

        Picasso.get().load(uri).into(itemHolder.photo);
        itemHolder.description.setText(photosList.get(i).getTitle());

        itemHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (itemClickListener != null) {
                    itemClickListener.onItemClick(photosList.get(i));
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return null != photosList ? photosList.size() : 0;
    }

    public class ItemHolder extends RecyclerView.ViewHolder {
        ImageView photo;
        TextView description;

        ItemHolder(View v) {
            super(v);
            photo = v.findViewById(R.id.flickr_image_tv_listdesign);
            description = v.findViewById(R.id.image_description_tv_listdesign);
        }
    }

    public interface ItemClickListener {
        void onItemClick(PhotoModel clickedPhoto);
    }
}