package com.benenesyildirim.flicktagram;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

public class DetailFragment extends Fragment {

    private String id,secret,title;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_detail, container, false);

        id = getArguments().getString("id");
        secret = getArguments().getString("secret");
        title = getArguments().getString("title");

        String uri = "https://farm5.staticflickr.com/4218/" + id + "_" + secret + ".jpg";

        ImageView photo = view.findViewById(R.id.flickr_image_detailfragment);
        TextView description = view.findViewById(R.id.image_description_tv_detailfragment);

        Picasso.get().load(uri).into(photo);
        description.setText(title);

        return view;
    }
}