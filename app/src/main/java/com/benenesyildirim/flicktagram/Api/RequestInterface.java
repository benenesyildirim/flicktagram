package com.benenesyildirim.flicktagram.Api;

import com.benenesyildirim.flicktagram.Model.MainData;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface RequestInterface {

    @GET("?method=flickr.photos.getRecent&api_key=b5fb8a415ec3179c454cad41c485a54d&format=json&nojsoncallback=1")
    Call<MainData> getRecentPhotos(@Query("page") int page);
}
