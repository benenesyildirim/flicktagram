package com.benenesyildirim.flicktagram.Model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class PhotoMainData {

    @SerializedName("photo")
    private List<PhotoModel> photo;

    public List<PhotoModel> getPhoto() {
        return photo;
    }

    public void setPhoto(List<PhotoModel> photo) {
        this.photo = photo;
    }
}
