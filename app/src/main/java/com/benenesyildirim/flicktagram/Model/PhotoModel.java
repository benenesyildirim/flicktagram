package com.benenesyildirim.flicktagram.Model;

import com.google.gson.annotations.SerializedName;

public class PhotoModel {

    @SerializedName("id")
    private String id;

    @SerializedName("secret")
    private String secret;

    @SerializedName("title")
    private String title;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSecret() {
        return secret;
    }

    public void setSecret(String secret) {
        this.secret = secret;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
