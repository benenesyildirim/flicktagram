package com.benenesyildirim.flicktagram.Model;

import com.google.gson.annotations.SerializedName;

public class MainData {

    @SerializedName("photos")
    private PhotoMainData photos;

    public PhotoMainData getPhotos() {
        return photos;
    }

    public void setPhotos(PhotoMainData photos) {
        this.photos = photos;
    }
}
